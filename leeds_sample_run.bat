@echo off

echo Installing dependencies...(0/1)

echo Downloading goodls...(1/1)
mkdir .\bin\goodls
curl.exe -L -o .\bin\goodls\goodls.exe https://github.com/tanaikech/goodls/releases/download/v1.2.7/goodls_windows_amd64.exe
echo Downloading goodls...Complete

echo Downloading sample content...(0/3)
mkdir .\sample_content

echo Downloading sample content: test-screen.webm...(1/3)
.\bin\goodls\goodls.exe -d .\sample_content -u https://drive.google.com/file/d/15wkBiDzUZqVTCz_Bt7kPsriRCXwD_zCz/
echo Downloading sample content: test-screen.webm...Complete

echo Downloading sample content: johnny-bubble.mp4...(2/3)
.\bin\goodls\goodls.exe -d .\sample_content -u https://drive.google.com/file/d/1tsfiLYwy_v5IKgxGbDdi2iCFjbucl3o6/
echo Downloading sample content: johnny-bubble.mp4...Complete

echo Downloading sample content: boidsv2.mkv...(3/3)
.\bin\goodls\goodls.exe -d .\sample_content -u https://drive.google.com/file/d/13op5y70WczMPk8NPDUrQv2J24xB44wr6/
echo Downloading sample content: boidsv2.mkv...Complete

echo Downloading sample content...Complete

echo Processing sample content...(0/3)

echo Running process-video.py on test-screen.webm...(1/3)
.\bin\python\WPy64-3950\python-3.9.5.amd64\python.exe .\process_video.py .\reports\leeds\display-config.json .\sample_content\test-screen.webm .\sample_content\test-screen-processed.webm -r 270
echo Running process-video.py on test-screen.webm...Complete

echo Running process-video.py on johnny-bubble.mp4...(2/3)
.\bin\python\WPy64-3950\python-3.9.5.amd64\python.exe .\process_video.py .\reports\leeds\display-config.json .\sample_content\johnny-bubble.mp4 .\sample_content\johnny-bubble-processed.mp4 -r 270
echo Running process-video.py on johnny-bubble.mp4...Complete

echo Running process-video.py on boidsv2.mkv...(3/3)
.\bin\python\WPy64-3950\python-3.9.5.amd64\python.exe .\process_video.py .\reports\leeds\display-config.json .\sample_content\boidsv2.mkv .\sample_content\boidsv2-processed.mkv -r 270
echo Running process-video.py on boidsv2.mkv...Complete

echo Processing sample content...Complete

echo Viewing processed sample content...(0/3)

echo Press any button to play test-screen-processed.webm...(1/3)
pause
echo Viewing test-screen-processed.webm...(1/3)
.\bin\mpv\mpv.exe .\sample_content\test-screen-processed.webm --no-border --loop=inf
echo Viewing test-screen-processed.webm...Complete

echo Press any button to play johnny-bubble-processed.mp4...(2/3)
pause
echo Viewing johnny-bubble-processed.mp4...(2/3)
.\bin\mpv\mpv.exe .\sample_content\johnny-bubble-processed.mp4 --no-border --loop=inf
echo Viewing johnny-bubble-processed.mp4...Complete

echo Press any button to play boidsv2-processed.mkv...(3/3)
pause
echo Viewing boidsv2-processed.mkv...(3/3)
.\bin\mpv\mpv.exe .\sample_content\boidsv2-processed.mkv --no-border --loop=inf
echo Viewing boidsv2-processed.mkv...Complete
pause

echo Viewing processed sample content...Complete

echo All modules finished.
pause
