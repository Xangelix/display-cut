# display-cut

Cuts and re-encodes a video to display on complex screen configurations.

## Installation

### Linux

1. Install the [Processing IDE](https://processing.org/download) for your platform.
2. Install [Python 3.9.5](https://www.python.org/downloads/)
3. Install the Python dependencies via `python -m pip install -r ./requirements.txt`.
4. Install ffmpeg to PATH.

### Windows

1. Install the [Processing IDE](https://processing.org/download) for your platform.
2. Run `.\install.bat`. This install will be fully portable (apart from the Processing IDE).

## Usage

### Leeds Demo (Windows only)

1. Run `.\leeds_sample_run.bat`. This will download three sample content videos and process them using the Leeds config standard with 270 degree rotation. MPV will be used to load each processed video, but you will still need to adjust the window to the screens. See this [video example](https://photos.app.goo.gl/BhWUtc1R7wwLvLhj7).

### Leeds Usage (Windows only)

1. To process the video, run from the `display-cut/` directory: `.\bin\python\WPy64-3950\python-3.9.5.amd64\python.exe .\process_video.py .\reports\leeds\display-config.json \path\to\input_video.ext \path\to\output_video.ext -r 270`
2. To play the video, run from the `display-cut/` directory: `.\bin\mpv\mpv.exe \path\to\output_video.ext --no-border --loop=inf`
3. You will then need to align and stretch the video manually.

### General Usage

1. Run `display_recorder.pde` using the downloaded Processing IDE.
2. Click first in the physical top left corner, then physical bottom right corner. If you are happy with the display you have draw, press `y`, otherwise press `n` to redraw the display.
3. Press `s` when you are happy with your drawn displays. You now have an outputted config file for the drawn displays which can be used with `process_video.py`.
4. A basic Linux command using PATH python: `python process_video.py ./reports/leeds/display-config.json input_video.mkv output_video.mkv -r 270`. A basic Windows command using local python: `.\bin\python\WPy64-3950\python-3.9.5.amd64\python.exe .\process_video.py .\reports\leeds\display-config.json .\sample_content\test-screen.webm .\sample_content\test-screen.webm -r 270`.
5. Many options are available for `process_video.py` including, encode crf, encode preset, display padding, forced rotation, crop-based or stretch-based aspect ratio compensation, splitting into multiple videos for each display, and debugging flags for not encoding and log verbosity. Help flag output:

```
$ ./process_video.py -h
usage: Convert videos to fit collected data.

positional arguments:
  display_config_json   collected screen data
  in_video              path of video to parse
  out_video             path of video to output

optional arguments:
  -h, --help            show this help message and exit
  --crf CRF             crf for intermediate encoding (default: 30)
  --preset PRESET       profile for intermediate encoding (default:
                        veryfast)
  -p PADDING, --padding PADDING
                        constant padding value (default: 0)
  -r ROTATE, --rotate ROTATE
                        add clockwise rotation degree (only 90, 180,
                        270 accepted)
  -c, --crop            crop for aspect ratio compensation
  -s, --split           keep video split for each screen
  -n, --noaction        do not encode
  -v, --verbose         print all actions
  -q, --quiet           hide all actions
```
6. This currently requires 3 encodes, therefore a low CRF and a slow preset should be specified to maintain video quality.

## Support

Please submit an issue [here](https://gitlab.com/Xangelix/display-cut/-/issues).

## Roadmap

Some `process_video.py` flags are yet to be implemented. See `-h` for more information.

If you have a feature request, please submit an issue [here](https://gitlab.com/Xangelix/display-cut/-/issues).

## Contributing

This project is open to contributions.

## Authors and acknowledgment

`display_recorder.pde` — Maansi Dasari \<mdbidda@gmail.com\>, mgcallanan \<marygcallanan@gmail.com\>, Cody Neiman \<cody-neiman@cody.to\>

`process_video.py` — Cody Neiman \<cody-neiman@cody.to\>

## License

This project is licensed under the [GNU General Public License v3.0](LICENSE).
